from setuptools import setup

setup(name='Website',
      version='1.0',
      description='martinkeefe.com App',
      author='Martin Keefe',
      author_email='budha57@hotmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['bottle', 'PyYAML'],
     )
