import os, yaml
from bottle import *

debug(True)

ROOT = os.path.dirname(__file__) + '/'
if ROOT == '/':
    ROOT = './'
print __file__, ROOT
TEMPLATE_PATH.insert(0,ROOT + 'views/')
app = Bottle()

EXT = {
    'html': '.html',
    'playlist': '.csv',
}

def get_page(name):
    try:
        f = open(ROOT + 'sitemap.yaml')
        pages = yaml.load(f)
        f.close()
    except Exception as ex:
        print ex
        pages = {}
    #print pages
    while name in pages and isinstance(pages[name], str):
        name = pages[name]
    if name in pages:
        page = pages[name]
        if '$' in page:
            for key in page['$']:
                page[key] = page['$'][key]
            del page['$']
        if 'type' not in page:
            page['type'] = 'html'
        if 'content' not in page:
            page['content'] = name + EXT[page['type']]
        page['name'] = name
        return page
    return None

def render_playlist(content, page):
    folder = 'http://' + page['bucket'] + '/' + os.path.basename(page['name']) + '/'
    if content:
        lines = content.split('\n')
        content = '<h2>' + page['title2'] + '</h2>\n'
        if 'intro' in page:
            content += '<p>' + page['intro'] + '</p>'
        content += '<ul class="playlist">\n'
        idx = 1
        for line in lines:
            name = format(idx,'02')
            row = line.split('\t')
            note = row[6] if len(row) > 6 and len(row[6]) > 0 else None
            content += '  <li'
            if idx % 2:
                content += ' class="odd">'
            else:
                content += ' class="even">'
            if note:
                content += '\n    <div id="sn-note-' + name
                content += '" class="snp-normal pinnable notedefault">'
                if 'note' in page:
                    #content += '<h1><a href="#" class="note-close">X</a>' + page['note'] + '</h1>'
                    content += '<h1>' + page['note'] + '</h1>'
                content += note
                content += '</div>'
            content += '<img src="' + folder + name + '.jpg"> '
            content += '<a href="' + folder + name + '.mp3"'
            if note:
                content += ' class="sn-hover-' + name + '"'
            content += '>\n    '
            content += '<div class="text">'
            content += '<big>' + name + '</big></br>'
            content += '<b>' + row[0] + '</b></br>'
            content += '"' + row[1] + '" '
            if row[2]:
                content += '(' + row[2] + ') '
            content += '</br><i>' + row[3] + '</i>'
            if row[4]:
                content += ' (' + row[4] + ')'
            content += ', ' + row[5]
            content += '</div>'
            content += '</a>'
            content += '</li>\n'
            idx += 1
        content += '</ul>\n'
    return content

def render(value, page):
    if isinstance(value,str):
        try:
            f = open(ROOT + 'content/' + value)
            content = f.read()
            f.close()
        except:
            content = ''
        if page['type'] == 'playlist':
            content = render_playlist(content, page)
    elif isinstance(value,dict):
        # Assume it's a sidebar
        #print page['name']
        #content = '<nav>\n'
        #if page['name'] != 'index':
        #    path = ''
        #    content += '<a href="/">home</a>\n'
        #    for crumb in page['name'].split('/')[:-1]:
        #        path += '/' + crumb
        #        if path != page['name']:
        #            content += ' / <a href="'+path+'">'+crumb+'</a>\n'
        #content += '</nav>'
        content = ''
        if 'intro' in value:
            content += '<h2 style="margin-bottom: 0;">' + value['title'] + '</h2>\n'
            content += '<p style="margin-top: 0; text-align: left; font-size: 90%;">' + value['intro'] + '</p>'
        else:
            content += '<h2>' + value['title'] + '</h2>\n'
        content += '<nav><ul>\n'
        for item in value['nav']:
            page = get_page(item)
            if page:
                title = page['title']
                if ' - ' in title:
                    titles = title.split(' - ')
                    title = titles[0]
                    if title == value['title']:
                        title = titles[1]
                content += '<li><a href="/' + item + '">' + title + '</a></li>\n'
        content += '</ul></nav>\n'
        #content += '<p class="foot">'
        #if 'foot' in value:
        #    content += value['foot'] + '\n'
        #content += 'Powered by <a href="http://bottlepy.org/docs/dev/">Bottle</a>.\n'
        #content += 'Hosted by <a href="https://www.openshift.com/">OpenShift</a>.\n'
        #content += 'Copyright &copy; 2011-2014 by Martin Keefe.</p>\n'

    return content

def entitle(page):
    page = page.copy()
    title = page['title']
    page['title'] = title
    page['title1'] = title
    if ' - ' in title:
        titles = title.split(' - ')
        page['title1'] = titles[0]
        page['title2'] = titles[1]
        title = titles[1]
    return page


@app.route('/static/<filename:path>')
def serve_static(filename):
    ext = filename[filename.rfind('.')+1:]
    return static_file(filename, root=ROOT+'static/')

@app.route('/')
@app.route('/index')
@view('front')
def serve_home():
    #print "Home!"
    return serve_dynamic()

@app.route('/<filename:path>')
@view('layout')
def serve_page(filename='index'):
    #print "Page " + filename 
    return serve_dynamic(filename)

def serve_dynamic(filename='index'):
    page = get_page(filename)
    #print filename, page
    if page:
        jsname = page['name'] + '.js'
        page = entitle(page)
        page['content'] = render(page['content'], page)
        page['sidebar'] = render(page['sidebar'], page)
        crumbs = '<nav class="crumbs">\n'
        if page['name'] != 'index':
            path = ''
            crumbs += '<a href="/">home</a>\n'
            for crumb in page['name'].split('/')[:-1]:
                path += '/' + crumb
                if path != page['name']:
                    crumbs += ' &raquo <a href="'+path+'">'+crumb+'</a>\n'
        crumbs += '</nav>'
        page['crumbs'] = crumbs
        try:
            #print ROOT + 'content/' + jsname
            f = open(ROOT + 'content/' + jsname)
            script = f.read()
            f.close()
            page['script'] = script
        except:
            pass
        #print(page['foot'])
        return page
    return HTTPError(code=404, output='Unknown page')

application = app

if __name__ == '__main__':
    debug(True)
    run(app=app, host='localhost', port=8080, reloader=True)
