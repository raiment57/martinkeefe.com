﻿<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title>Martin's {{title or "Stuff"}}</title>
  <meta name="description" content="">
  <meta name="author" content="Martin Keefe">
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <link rel="stylesheet" href="/static/css/main.css">
  %for name in get('styles', []):
    <link rel="stylesheet" href="/static/css/{{name}}.css">
  %end
  %if defined('jui'):
    <!--link rel=stylesheet href="http://code.jquery.com/ui/1.10.3/themes/start/jquery-ui.css"/-->
    <!--link rel=stylesheet href="/static/css/AfterDark/afterwork.css"/-->
    <link rel=stylesheet href="/static/css/jui.css"/>
  %end
  <script src="/static/js/libs/modernizr-2.6.2.min.js"></script>
</head>
<body>
  <!--[if lt IE 7]>
	<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
  <![endif]-->

  <div id="container">
    <div id="sidenav">
        <!--Fixed Sidenav Goes Here-->
        <header>
            <a href="/"><img src="/static/img/flamingtext_23105419589239821.png" alt="Martin's Stuff"></a>
        </header>
        {{! sidebar }}
    </div> 
    <div id="content">
      <article>
        <header>
            {{! crumbs }}
            <h1>{{title1}}</h1>
        </header>
        <!--Content Goes Here-->
        {{! content }}
      </article>
      <p class="foot clear">Copyright &copy; 2011-2014 by Martin Keefe.</p>
    </div>
  </div> <!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="/static/js/libs/jquery-1.9.1.min.js"><\/script>')</script>
  <!--script src="/static/js/plugins.js"></script-->
  <script src="http://documentcloud.github.io/underscore/underscore-min.js"></script>
  <script data-main="/static/js/mylibs/" src="/static/js/libs/require.js"></script>
  
%if defined('jui'):
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> 
%end  
%if defined('code'):
  <script src="/static/js/libs/highlight/highlight.js"></script>
  %for name in code:
    <script src="/static/js/libs/highlight/languages/{{name}}.js"></script>
    % if name == 'coffeescript':
    <script src="/static/js/highlight-coffee.js"></script>
    %end
  %end
    <script>
      hljs.tabReplace = '    ';
      hljs.initHighlightingOnLoad();
    </script>
%end
%for name in get('libs', []):
  <script src="/static/js/libs/{{name}}.js"></script>
%end
%if defined('three'):
  <script src="https://rawgithub.com/mrdoob/three.js/master/build/three.min.js"></script> 
  <script src="/static/js/animator.js"></script> <!-- https://github.com/erik-landvall/animator -->
  <!-- script src="/static/js/Detector.js"></script -->
  <script src="/static/js/TrackballControls.js"></script>
%end  
%if defined('math'):
  <script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
  <script type="text/javascript">
    MathJax.Hub.Config({
      extensions: ["tex2jax.js"],
      "HTML-CSS": { scale: 100, availableFonts: [], webFont: "TeX"}
    });
  </script>
%end
      <script src="/static/js/main.js"></script>
%for name in get('scripts', []):
  <script src="/static/js/{{name}}.js"></script>
%end
%if defined('script'):
  <script type="text/javascript">
    {{! script }}
  </script>
%end  
</body>
</html>
