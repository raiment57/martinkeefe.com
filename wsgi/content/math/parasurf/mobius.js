function mobius(u, v, p) {
    var PI = Math.PI,
        SQRT2 = Math.SQRT2,
        cos = Math.cos,
        sin = Math.sin;
    return {
        calc: function(ur, vr, p, t) { 
            function func(_u, _v) {
                var u = ur.off + _u * ur.scale,
                    v = vr.off + _v * vr.scale,
                    x = cos(u) + v*cos(u/2)*cos(u),
                    y = sin(u) + v*cos(u/2)*sin(u),
                    z = v*sin(u/2);
                return [x, y, z];
            }
            return calcParametricGeometry(func, ur, vr, t);
        },
        domain: [ 
            {name:'u', min:-PI, max:PI, steps:60, pi:true},
            {name:'v', min:-1.3, max:1.3, steps:8},
        ],
        param: [
        ],
        view: {
            distance: 2.5,
        },
    };
}

$().ready(function () {
    parasurf('fig1', mobius);
});

/*
f(u, v) = ( (cos(u) + v*cos(u/2)*cos(u)), (sin(u) +
v*cos(u/2)*sin(u)), v*sin(u/2)),

0 <= u <= 2*pi, -0.3 <= v <= 0.3 
*/