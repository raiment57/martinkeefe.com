function limpetTorus(u, v, p) {
    var PI = Math.PI,
        SQRT2 = Math.SQRT2,
        cos = Math.cos,
        sin = Math.sin;
    return {
        calc: function(ur, vr, p, t, ff) { 
            function func(_u, _v) {
                var u = ur.off + _u * ur.scale,
                    v = vr.off + _v * vr.scale,
                    x = -cos(u) / (SQRT2 + sin(v)),
                    y = sin(u) / (SQRT2 + sin(v)),
                    z = 1 / (SQRT2 + cos(v));
                return [x, y, z];
            }
            return calcParametricGeometry(func, ur, vr, t, ff);
        },
        domain: [ 
            {name:'u', min:-PI, max:PI, steps:60, pi:true},
            {name:'v', min:-PI, max:PI, steps:60, pi:true},
        ],
        param: [
        ],
    };
}

$().ready(function () {
    parasurf('fig1', limpetTorus);
});
