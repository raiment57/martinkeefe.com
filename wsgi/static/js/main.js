//if (typeof(hljs)!=='undefined') {
//    hljs.initHighlightingOnLoad();
//}

//var CONTENT_WIDTH = 692;

function fzip(f) {
    var args = _.zip.apply(null, slice.call(arguments, 1));
    return args.map(function (arg) { return f.apply(null, arg); });
}
function lerp(a, b, t) {
	return (t - 1) * a + t * b;
}
function izip(list) {
    return _.zip(_.range(list.length), list);
}
function imap(list, fn) { 
    return _.map(izip(list), function (pair) { return fn(pair[0], pair[1]); });
}
function ieach(list, fn) { 
    _.each(izip(list), function (pair) { return fn(pair[0], pair[1]); });
}

$(function() {
    var browserIE6 = (navigator.userAgent.indexOf("MSIE 6")>=0) ? true : false; //Check for IE6
    var sidenavHeight = $("#sidenav").height(); //Get height of sidenav
    
    function staticNav() {
        var winHeight = $(window).height(); //Get height of viewport
        var contentHeight = $("#content").height(); 
        //console.log( contentHeight );

        if (browserIE6) { //if IE6...
            $("#sidenav").css({'position' : 'absolute'});  //reset the sidenav to be absolute
        } else { //if not IE6...
            $("#sidenav").css({'position' : 'fixed'}); //reset the sidenav to be fixed
        }

        if (sidenavHeight > winHeight) { //If sidenav is taller than viewport...
            $("#sidenav").css({'position' : 'static'}); //switch the fixed positioning to static. Say good bye to sticky nav!
        }
    }
    
    $(window).resize(function () { //Each time the viewport is adjusted/resized, execute the function
        staticNav();
    });

    staticNav(); //Execute function on load
});

