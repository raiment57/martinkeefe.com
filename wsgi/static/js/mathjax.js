/* Author:

*/

function syms(i, elem) {
    var text = elem.innerHTML;
    var html = '<table class="centre">';
    jQuery.each(text.split('\n'), function (i, line) {
        html += '<tr>';
        jQuery.each(line.split(' '), function (i, code) {
            if (code === '_') {
                html += '<td></td><td></td>';
            } else if (code !== '') {
                html += '<td class="sym">\\('+code+'\\)</td><td class="code"><pre><code>'+code+'</code></pre></td>';
            }
        });
        html += '</tr>';
    });
    html += '</table>';
    elem.innerHTML = html;
}

$(function() {
    $('div.syms').each(syms);
    $('tex').each(function (i, elem) {
        $(elem).replaceWith('<pre class="code"><code class="tex">'+elem.innerHTML+'</code></pre>');
    });
});



