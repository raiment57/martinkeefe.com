soundManager.setup({
  url: '/static/swf/',
  flashVersion: 9, // optional: shiny features (default = 8)
  //useFlashBlock: false, // optionally, enable when you're ready to dive in
  /**
   * read up on HTML5 audio support, if you're feeling adventurous.
   * iPad/iPhone and devices without flash installed will always attempt to use it.
   */
  //onready: function() {
    // Ready to use; soundManager.createSound() etc. can now be called.
  //}
});

var supernote = new SuperNote('sn', {
    cssVis: 'visible'
});
